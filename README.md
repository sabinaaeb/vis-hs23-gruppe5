# Vis HS23 Gruppe5

Visualisierungsprojekt von Sabina Aebersold, Sandra Hiltbrunner, Patricia Schneider, Martina Zwick

Dashboard mit Daten zur Kinostatistik in der Schweiz zwischen 01.01.2019 und 31.12.2022

Datenquelle: https://www.bfs.admin.ch/asset/de/ts-x-16.02.01-01

Quellcode: https://gitlab.com/sabinaaeb/vis-hs23-gruppe5 

Dashboard abrufbar unter: http://visgruppe5.pythonanywhere.com/
