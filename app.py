from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
from io import StringIO
import requests

# Template für alle Diagramme festlegen
layout = "ggplot2"

# Funktion zum Importieren von Daten aus CSV-Datei
def import_data(file_path):
    # CSV in DataFrame umwandeln
    df_csv = pd.read_csv(file_path, delimiter=";")
    df_csv['date'] = pd.to_datetime(df_csv['date'], format='%d.%m.%Y')
    return df_csv

# Funktion zum Datenformat festlegen
def format_date(date):
    return pd.to_datetime(date, format='%Y-%m-%d').strftime("%d.%m.%Y")

# Funktion zum Generieren des Kuchendiagramms
def generate_pie_chart(df, start_date, end_date, legend_labels, color_scheme):
    # Datenfilterung basierend auf ausgewähltem Datum, Einheit und Ursprung
    df_filtered = df[(df['date'] >= start_date) & (df['date'] <= end_date) & (df['unit'] == 'prj') & (df['origin'].isin(['ous', 'oeu', 'och', 'oot']))]
    
    # Legende zuweisen
    df_filtered['origin'] = df_filtered['origin'].map(legend_labels)
    
    # start_date und end_date umformatieren
    start_date_formatted = format_date(start_date)
    end_date_formatted = format_date(end_date)
    
    # Kuchendiagramm generieren
    return px.pie(df_filtered, values='value', names="origin", 
                  title=f'Anteile Filmvorführungen nach Herkunft ({start_date_formatted} - {end_date_formatted}) ', 
                  color_discrete_sequence=color_scheme[1:]
                  ).update_layout(template=layout,font=dict(size=8))

# Funktion zum Generieren des Liniendiagramms
def generate_line_chart(df, start_date, end_date, legend_labels, color_scheme):
    # Datenfilterung basierend auf ausgewähltem Datum, Einheit und Ursprung
    df_filtered = df[(df['date'] >= start_date) & (df['date'] <= end_date) & (df['unit'] == 'adm') & (df['recent'] == 'rall')]
    
    # Convert start_date and end_date to datetime objects
    start_date_formatted = format_date(start_date)
    end_date_formatted = format_date(end_date)
    
    # Legende zuweisen
    df_filtered['origin'] = df_filtered['origin'].map(legend_labels)

    # Liniendiagramm generieren
    return px.line(df_filtered, x="date", y="value", line_group='origin',
                   color='origin', markers=True,
                   labels={'date': 'Datum', 'value': 'Kinoeintritte', 'origin': 'Produktionsland'},
                   category_orders={"origin": ["Alle", "USA", "Europa", "Schweiz", 'andere']},
                   color_discrete_sequence=color_scheme,
                   title=f'Kinoeintritte pro Woche ({start_date_formatted} - {end_date_formatted})').update_layout(
                       template=layout,
                       yaxis=dict(tickformat=',d'),
                       font=dict(size=8)
                   )

# Funktion zum Generieren des Balkendiagramms
def generate_bar_chart(df, start_date, end_date, color_scheme):
    # Datenfilterung basierend auf ausgewähltem Datum, Einheit und Ursprung
    df_filtered = df[(df['date'] >= start_date) & (df['date'] <= end_date) & (df['unit'] == 'cin') & (df['recent'] == 'rall') & (df['origin'] == 'oall')]
    
    # Convert start_date and end_date to datetime objects
    start_date_formatted = format_date(start_date)
    end_date_formatted = format_date(end_date)
    
    # Balkendiagramm generieren
    return px.bar(df_filtered, x='year', y='value', 
                  labels={
                      'value': 'Anzahl aktiver Kinos', 
                          'year': 'Jahr'
                  },
                  title=f'Anzahl aktiver Kinos pro Jahr (Durchschnittswert) ({start_date_formatted} - {end_date_formatted})',
                  color_discrete_sequence=color_scheme,
                  barmode='group'
                  ).update_layout(template=layout,
                                  yaxis = dict(tickformat=',d'),
                                  font=dict(size=8))

# Funktion zum Generieren des Scatter-Plots
def generate_scatter_plot(df, start_date, end_date, color_scheme, trendline_option, event_options):
    # Kopie des DataFrames erstellen und 'year' in einen String konvertieren
    df_scatter = df.copy()
    df_scatter['year'] = df_scatter['year'].astype(str)
    
    # Datenfilterung basierend auf ausgewähltem Datum, Einheit, kürzlichem Wert und Berechnung des Mittelwert für jedes Jahr und jede Woche
    df_filtered = df_scatter[(df_scatter['date'] >= start_date) & (df_scatter['date'] <= end_date) & (df_scatter['unit'] == 'adm') & (df_scatter['recent'] == 'rall')]
    df_filtered = df_filtered.groupby(['year', 'week'], as_index=False).agg({'value': 'mean', 'comment': 'first'})
    
    # Convert start_date and end_date to datetime objects
    start_date_formatted = format_date(start_date)
    end_date_formatted = format_date(end_date)
    
    # Scatter-Plot generieren
    fig = px.scatter(df_filtered, y="value", x="week", color="year", trendline=trendline_option,
                 labels={'value': 'Anzahl Eintritte', 'week': 'Kinowoche', 'year': 'Jahr'},
                 color_discrete_sequence=color_scheme,
                 title=f'Kinoeintritte pro Woche ({start_date_formatted} - {end_date_formatted})').update_layout(
                    template=layout,
                    yaxis=dict(tickformat=',d'),
                    font=dict(size=8)
                 )
    # Überprüfen, ob Checkbox für Ereignisse aktiviert ist
    if event_options == True:
        # Labels aus der Spalte "comment" für alle Punkte hinzufügen
        for index, row in df_filtered.iterrows():
            # Nur Annotation hinzufügen, wenn die Kommentarspalte nicht leer ist
            if pd.notna(row['comment']):
                fig.add_annotation(x=row['week'], y=row['value'],
                                text=row['comment'], showarrow=True,
                                arrowhead=1, arrowcolor='rgba(255,255,255,0.7)',
                                font=dict(size=8), ax=0, ay=-20)
                
                # Benutzerdefinierte Markierung (in diesem Fall Stern) für Datenpunkt mit Annotation hinzufügen
                fig.add_trace(go.Scatter(x=[row['week']], y=[row['value']],
                                        mode='markers',
                                        marker=dict(symbol='star', size=15, color='rgba(255,255,0,0.7)', line=dict(width=0)),  # Änderung hier
                                        showlegend=False,
                                        hoverinfo='y'))  # Hier hoverinfo auf 'y' setzen

    return fig

app = Dash(__name__, title='Kinodaten Schweiz')

# Stylesheet verknüpfen
app.css.append_css({"external_url": "/assets/style.css"})

# Daten importieren, Legende definieren 
df_csv = import_data("data/kinostatistik_datensatz_bereinigt.csv")
legend_labels = {'oall': 'Alle', 'ous': 'USA', 'oeu': 'Europa', 'och': 'Schweiz', 'oot': 'andere'}


# Farbgebung für Dropdown
# Quellen für die Erstellung der Farbschemas: https://tailwindcss.com/docs/customizing-colors und 
# https://colorbrewer2.org/#type=diverging&scheme=RdYlBu&n=7 
farbpalette = {'Rot': ['#dc2626', '#f97316', '#eab308', '#4f46e5', '#db2777',  '#c026d3'], 
               'Grün': ['#a3e635', '#4ade80', '#facc15', '#c084fc', '#e879f9', '#c026d3'],
               'Blau': ['#3b82f6', '#38bdf8', '#06b6d4', '#10b981', '#84cc16', '#f59e0b'],
               'Gelb': ['#facc15', '#fb923c', '#fb7185', '#e879f9', '#a78bfa', '#818cf8']                
               }

# Default Farbe definieren
selected_color = 'Gelb'

# Template und Farbe für alle Diagramme festlegen
layout = "ggplot2"
color_scheme = farbpalette[selected_color]

# App-Layout
app.layout = html.Div([
    html.Div(className='container', children=[
        html.Div(className='infobox', children=[
            html.H1("Kinodaten Schweiz"),
            html.Div(id='filter', children=[
                html.H2("Filter nach Zeitraum"),
                "Wähle einen Zeitraum zwischen 01.01.2019 - 31.12.2022",
                dcc.DatePickerRange(
                    id='zeitraum-datepicker',
                    display_format='DD.MM.YYYY',
                    start_date='2019-01-01',
                    end_date='2022-12-31',
                    className='datepicker'
                ),
                
            ]),
            html.H2("Farbskala"),
            dcc.Dropdown(id='farbe_dropdown', options=[{'label': key, 'value': key} for key in farbpalette.keys()], value='Gelb'),
            html.Div(id='reference', children=[
                html.H2("Infos"),
                "Kinodaten der Schweiz vom 01.01.2019 bis 31.12.2022",
                html.Br(),
                "Datenquelle: ",
                html.A("Bundesamt für Statistik", href="https://www.bfs.admin.ch/asset/de/ts-x-16.02.01-01"), " zugegriffen am 13.11.2023",
                html.Br(),
                html.Br(),
                "Diese Visualisierung entstand im Modul VIS an der Fachhochschule Graubünden",
                html.Br(),
                html.Br(),
                "(c) Sabina Aebersold, Sandra Hiltbrunner, Patricia Schneider, Martina Zwick",
            ]),
        ]),
        html.Div(className='charts', children=[
            html.Div(id='row1', className='row', children=[
                dcc.Graph(id="line-chart", className='chart'),
            ]),            

        html.Div(id='row2', className='row', children=[
            dcc.Graph(id="scatter-plot", className='chart', figure={}, config={'scrollZoom': False})]),
        html.Div(id="checklist_scatter", children=[
            dcc.Checklist(id="checklist", options=[
                {'label': 'Trendlinie pro Jahr anzeigen', 'value': 'trendline'},
                {'label': 'Ereignisse anzeigen', 'value': 'events'},
                ], value=['trendline', 'events']),
        ]),

        html.Div(id='row3', className='row', children=[
            dcc.Graph(id="pie-chart", className='chart'),
            dcc.Graph(id="bar-chart", className='chart'),
        ])
        ])
    ])
])

# Callbacks zum Aktualisieren der Diagramme 
@app.callback(
    Output("pie-chart", "figure"), 
    [Input("zeitraum-datepicker", "start_date"), 
     Input("zeitraum-datepicker", "end_date"), 
     Input("farbe_dropdown", "value")])
def update_pie_chart(start_date, end_date, selected_color):
    color_scheme = farbpalette[selected_color]
    return generate_pie_chart(df_csv, start_date, end_date, legend_labels, color_scheme)

@app.callback(Output("line-chart", "figure"), 
              [Input("zeitraum-datepicker", "start_date"), 
               Input("zeitraum-datepicker", "end_date"), 
               Input("farbe_dropdown", "value")])
def update_line_chart(start_date, end_date, selected_color):
    color_scheme = farbpalette[selected_color]
    return generate_line_chart(df_csv, start_date, end_date, legend_labels, color_scheme)

@app.callback(Output("bar-chart", "figure"), 
              [Input("zeitraum-datepicker", "start_date"), 
               Input("zeitraum-datepicker", "end_date"), 
               Input("farbe_dropdown", "value")])
def update_bar_chart(start_date, end_date, selected_color):
    color_scheme = farbpalette[selected_color]
    return generate_bar_chart(df_csv, start_date, end_date, color_scheme)

@app.callback(Output("scatter-plot", "figure"), 
              [Input("zeitraum-datepicker", "start_date"), 
               Input("zeitraum-datepicker", "end_date"), 
               Input("farbe_dropdown", "value"),
               Input("checklist", "value")])
def update_scatter_plot(start_date, end_date, selected_color, options):
    color_scheme = farbpalette[selected_color]
    # Trendlinie ein oder ausblenden
    trendline_option = "lowess" if 'trendline' in options else None
    # Überprüfung,  ob Check-Box für Ereignisse aktiviert ist
    event_options = True if 'events' in options else False
    return generate_scatter_plot(df_csv, start_date, end_date, color_scheme, trendline_option, event_options)

# Start der Anwendung
if __name__ == '__main__':
    app.run_server(debug=False, port=8051)
